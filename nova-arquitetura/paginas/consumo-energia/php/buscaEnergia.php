<?php
  require_once('../../Dbconfig.php');
  require_once('../../Sessao.php');
?>
<?php
$residencia = $_POST['residencia'];

header('Content-Type: application/json');

$mysqli = new mysqli($servidor, $usuario_banco, $senha_banco, $banco);

if(!$mysqli){
  die("Connection failed: " . $mysqli->error);
}


$query = sprintf("SELECT sum(aparato_consumo) as aparato_consumo from aparato_has_residencia
 join aparato on aparato_has_residencia.aparato_id = aparato.aparato_id
where residencia_id = $residencia");


$result = $mysqli->query($query);


$data = array();
foreach ($result as $row) {
  $data[] = $row;
}


$result->close();


$mysqli->close();


print json_encode($data);

?>