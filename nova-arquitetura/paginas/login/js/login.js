$(document).ready(function(){
	$("#bLogin").click(function(){
		var usuario = $("#usuario").val();
		var senha = $("#senha").val();

		fLocalComunicaServidor(usuario, senha);
		return false;
	});
});

function fLocalComunicaServidor(usuario, senha){
	$.ajax({
		type: "POST",
		url: "php/valida-login.php",
		data:{
			usuario:usuario,
			senha: senha
		},
		success:function(data){
			if(data == "true"){
				window.location.href="../home/#";
			}else{
				alert("senha/email errado ou email não confirmado");
				window.location.href="login.html";
			}
		}
	});
}
