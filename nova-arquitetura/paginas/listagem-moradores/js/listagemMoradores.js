$(document).ready(function(){
    


    $.ajax({
        type: "POST",
        url: "php/listagemMoradores.php",
        success:function(data){
            var obj = JSON.parse(data);
            console.log(obj);
            $.each(obj,function(i, itens){
                var element = document.getElementById('divRes');
                
                var habitante_id;
                var hab;
                $.each(itens.habitantes, function(x, item){
                    if(item.habitatante_sexo == 'F'){
                        item.habitante_sexo="Feminino";
                    }else{
                        item.habitante_sexo="Masculino";
                    }
                    
                    hab = item.habitante_nome + "<br>" + item.habitante_sexo +"<br>"+ item.habitante_nascimento;
                    habitante_id = item.habitante_id;
                });
                    console.log(hab);
                var residencia = [];
                $.each(itens.residencia, function(x,item){
                    //console.log(item.habitante_nome);
                    var concat = item.residencia_rua + ", "+ item.residencia_numero + " - " + item.residencia_complemento;
                    residencia.push(concat);
                    //divHab.innerHTML += "<div class='row'>"+item.habitante_nome+"</div>";
                    
                });
                console.log(residencia);
    
                var eletrodomesticos = [];
                $.each(itens.aparatos, function(x,item){
                    eletrodomesticos.push(item.aparato_descricao + " - " + item.aparato_consumo+"W");
                });
                console.log(eletrodomesticos);
                
                
                
                element.innerHTML += "<div id='home'><div class='row'><h4>"+hab +"</h4></div>";
                
                element.innerHTML += "<h5>Residencia(s):</h5>"
                for(var i =0; i<residencia.length; i++){
                    var res = " \" "+ residencia[i] + " \" ";
                    element.innerHTML += "<span id='"+residencia[i]+habitante_id +"' onclick='apagarMorador("+ res+","+ habitante_id+");' value='"+habitante_id +"'>" + residencia[i] + "</span><br>";
                }
                element.innerHTML +="<br>";
                element.innerHTML += "<h5>Aparatos Domésticos:</h5>"
                for(var i =0; i<eletrodomesticos.length; i++){
                    element.innerHTML += eletrodomesticos[i];
                }
                element.innerHTML +="<br><br>";
                
                function FormataStringData(data) {
                    var dia  = data.split("/")[0];
                    var mes  = data.split("/")[1];
                    var ano  = data.split("/")[2];
                  
                    return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
                    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
                  }
            });
            
        }
    });
    
    
});

