$(document).ready(function(){
    $("#bCadastro").click(function(e){
        event.preventDefault(e);
        var rua = $("#rua").val();
        var numero = $("#numero").val();
        var e = $("#cidades");
        var cidade = $('#cidades').find(":selected").val();
        var complemento = $("#complemento").val();
        var selectedVal = new Array();
        $.each($("#checkDiv input[type='checkbox']:checked"), function(){
            selectedVal.push($(this).val());
        });
        
        $.ajax({
            type: "POST",
            url: "php/cadastroResidencia.php",
            data:{
                rua:rua,
                numero:numero,
                cidade:cidade,
                complemento:complemento,
                selectedVal:selectedVal
            },
            success:function(data){
                console.log(data);
                alert("Residencia cadastrada com sucesso!");
                window.location.href="../home/#";
            }
        });
    });
});