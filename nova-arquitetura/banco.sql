CREATE DATABASE  IF NOT EXISTS `db_experiencia` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_experiencia`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: db_experiencia
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agua`
--

DROP TABLE IF EXISTS `agua`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `agua` (
  `agua_id` int(11) NOT NULL AUTO_INCREMENT,
  `agua_data` date DEFAULT NULL,
  `agua_consumo` double DEFAULT NULL,
  `agua_residencia_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`agua_id`),
  KEY `agua_residencia_id` (`agua_residencia_id`),
  CONSTRAINT `agua_ibfk_1` FOREIGN KEY (`agua_residencia_id`) REFERENCES `residencia` (`residencia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agua`
--

LOCK TABLES `agua` WRITE;
/*!40000 ALTER TABLE `agua` DISABLE KEYS */;
/*!40000 ALTER TABLE `agua` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aparato`
--

DROP TABLE IF EXISTS `aparato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `aparato` (
  `aparato_id` int(11) NOT NULL AUTO_INCREMENT,
  `aparato_descricao` varchar(100) DEFAULT NULL,
  `aparato_consumo` double DEFAULT NULL,
  `apar_user_id` int(11) DEFAULT NULL,
  `apar_domestico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`aparato_id`),
  KEY `apar_user_id` (`apar_user_id`),
  KEY `aparato_ibfk_4_idx` (`apar_domestico_id`),
  CONSTRAINT `aparato_ibfk_3` FOREIGN KEY (`apar_user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `aparato_ibfk_4` FOREIGN KEY (`apar_domestico_id`) REFERENCES `aparatodomestico` (`aparatodomestico_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aparato`
--

LOCK TABLES `aparato` WRITE;
/*!40000 ALTER TABLE `aparato` DISABLE KEYS */;
INSERT INTO `aparato` VALUES (1,'tele',123,2,1),(2,'gela',123,2,2),(3,'COMP',1234,2,3),(4,'LAVA',123,2,4),(5,'VIDEO',1234,2,6),(6,'AWDWD1Q',123,2,4),(7,'AWDSDWASDW',213445,2,3),(8,'daiwjdi',1234,2,5),(9,'QWERTY',789,2,5),(10,'PLAYSTATION',1334,2,6),(11,'Teve tunada',2000,2,1),(12,'frigobar lindo',4000,2,5),(13,'Geladeira nova',400,2,2),(14,'Roupas',256,2,4),(15,'computador',123,2,3),(16,'minha geladeira',300,2,2),(17,'AAAAAAAAAAAAAAA',134,2,1),(18,'555',123,2,2),(19,'raaatatwer',444,2,1),(20,'wdawd',123,2,1),(21,'Brastemp',500,6,2);
/*!40000 ALTER TABLE `aparato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aparato_has_habitante`
--

DROP TABLE IF EXISTS `aparato_has_habitante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `aparato_has_habitante` (
  `aparato_id` int(11) DEFAULT NULL,
  `habitante_id` int(11) DEFAULT NULL,
  KEY `aparato_id` (`aparato_id`),
  KEY `habitante_id` (`habitante_id`),
  CONSTRAINT `aparato_has_habitante_ibfk_1` FOREIGN KEY (`aparato_id`) REFERENCES `aparato` (`aparato_id`),
  CONSTRAINT `aparato_has_habitante_ibfk_2` FOREIGN KEY (`habitante_id`) REFERENCES `habitante` (`habitante_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aparato_has_habitante`
--

LOCK TABLES `aparato_has_habitante` WRITE;
/*!40000 ALTER TABLE `aparato_has_habitante` DISABLE KEYS */;
INSERT INTO `aparato_has_habitante` VALUES (10,4),(10,5),(14,4),(15,4),(15,5),(16,4),(18,4);
/*!40000 ALTER TABLE `aparato_has_habitante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aparato_has_residencia`
--

DROP TABLE IF EXISTS `aparato_has_residencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `aparato_has_residencia` (
  `aparato_id` int(11) DEFAULT NULL,
  `residencia_id` int(11) DEFAULT NULL,
  KEY `aparato_has_residencia_ibfk_1` (`aparato_id`),
  KEY `aparato_has_residencia_ibfk_2` (`residencia_id`),
  CONSTRAINT `aparato_has_residencia_ibfk_1` FOREIGN KEY (`aparato_id`) REFERENCES `aparato` (`aparato_id`) ON DELETE CASCADE,
  CONSTRAINT `aparato_has_residencia_ibfk_2` FOREIGN KEY (`residencia_id`) REFERENCES `residencia` (`residencia_id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aparato_has_residencia`
--

LOCK TABLES `aparato_has_residencia` WRITE;
/*!40000 ALTER TABLE `aparato_has_residencia` DISABLE KEYS */;
INSERT INTO `aparato_has_residencia` VALUES (10,17),(11,17),(12,17),(13,NULL),(14,17),(15,17),(20,17),(21,20);
/*!40000 ALTER TABLE `aparato_has_residencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aparatodomestico`
--

DROP TABLE IF EXISTS `aparatodomestico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `aparatodomestico` (
  `aparatodomestico_id` int(11) NOT NULL AUTO_INCREMENT,
  `aparato_nome` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`aparatodomestico_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aparatodomestico`
--

LOCK TABLES `aparatodomestico` WRITE;
/*!40000 ALTER TABLE `aparatodomestico` DISABLE KEYS */;
INSERT INTO `aparatodomestico` VALUES (1,'TELEVISAO'),(2,'GELADEIRA'),(3,'COMPUTADOR'),(4,'LAVA-ROUPAS'),(5,'FRIGOBAR'),(6,'VIDEOGAME');
/*!40000 ALTER TABLE `aparatodomestico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cidade` (
  `cidade_id` int(11) NOT NULL AUTO_INCREMENT,
  `cidade_nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cidade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` VALUES (1,'Curitiba'),(2,'Sao Paulo');
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `combustivel`
--

DROP TABLE IF EXISTS `combustivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `combustivel` (
  `combustivel_id` int(11) NOT NULL AUTO_INCREMENT,
  `combustivel_data` date DEFAULT NULL,
  `combustivel_consumo` double DEFAULT NULL,
  `comb_residencia_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`combustivel_id`),
  KEY `comb_residencia_id` (`comb_residencia_id`),
  CONSTRAINT `combustivel_ibfk_1` FOREIGN KEY (`comb_residencia_id`) REFERENCES `residencia` (`residencia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `combustivel`
--

LOCK TABLES `combustivel` WRITE;
/*!40000 ALTER TABLE `combustivel` DISABLE KEYS */;
/*!40000 ALTER TABLE `combustivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumo_combustivel`
--

DROP TABLE IF EXISTS `consumo_combustivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consumo_combustivel` (
  `veiculo_id` int(11) DEFAULT NULL,
  `horas` double DEFAULT NULL,
  KEY `veiculo_id` (`veiculo_id`),
  CONSTRAINT `consumo_combustivel_ibfk_1` FOREIGN KEY (`veiculo_id`) REFERENCES `veiculo` (`veiculo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumo_combustivel`
--

LOCK TABLES `consumo_combustivel` WRITE;
/*!40000 ALTER TABLE `consumo_combustivel` DISABLE KEYS */;
INSERT INTO `consumo_combustivel` VALUES (1,3),(2,2),(1,6),(3,3);
/*!40000 ALTER TABLE `consumo_combustivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumo_energia`
--

DROP TABLE IF EXISTS `consumo_energia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consumo_energia` (
  `consumo_energia_id` int(11) NOT NULL AUTO_INCREMENT,
  `horas` double DEFAULT NULL,
  `aparato_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`consumo_energia_id`),
  KEY `aparato_id` (`aparato_id`),
  CONSTRAINT `consumo_energia_ibfk_1` FOREIGN KEY (`aparato_id`) REFERENCES `aparato` (`aparato_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumo_energia`
--

LOCK TABLES `consumo_energia` WRITE;
/*!40000 ALTER TABLE `consumo_energia` DISABLE KEYS */;
INSERT INTO `consumo_energia` VALUES (1,2,1),(2,7,3),(3,8,4),(4,5,2),(5,4,1),(6,5,10),(7,5,10),(8,12,12),(9,20,13),(10,4,1),(11,20,1),(12,4,1),(13,5,21);
/*!40000 ALTER TABLE `consumo_energia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `energia`
--

DROP TABLE IF EXISTS `energia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `energia` (
  `energia_id` int(11) NOT NULL AUTO_INCREMENT,
  `energia_data` date DEFAULT NULL,
  `energia_consumo` double DEFAULT NULL,
  `ene_residencia_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`energia_id`),
  KEY `ene_residencia_id` (`ene_residencia_id`),
  CONSTRAINT `energia_ibfk_1` FOREIGN KEY (`ene_residencia_id`) REFERENCES `residencia` (`residencia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `energia`
--

LOCK TABLES `energia` WRITE;
/*!40000 ALTER TABLE `energia` DISABLE KEYS */;
/*!40000 ALTER TABLE `energia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `habitante`
--

DROP TABLE IF EXISTS `habitante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `habitante` (
  `habitante_id` int(11) NOT NULL AUTO_INCREMENT,
  `habitante_nome` varchar(50) DEFAULT NULL,
  `habitante_nascimento` date DEFAULT NULL,
  `habitante_genero` varchar(1) DEFAULT NULL,
  `hab_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`habitante_id`),
  KEY `hab_user_id` (`hab_user_id`),
  CONSTRAINT `habitante_ibfk_1` FOREIGN KEY (`hab_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habitante`
--

LOCK TABLES `habitante` WRITE;
/*!40000 ALTER TABLE `habitante` DISABLE KEYS */;
INSERT INTO `habitante` VALUES (1,'Bruno','2019-11-11','M',1),(2,'Pedro','2017-11-11','M',1),(3,'$nome','1999-11-11','F',1),(4,'dawd','0123-12-11','M',2),(5,'Joao','2005-11-11','M',2),(6,'Gilson','1111-11-11','M',2),(7,'Gilson 2','2344-11-11','M',2),(8,'João','1456-11-11','M',2),(9,'Guilherme','2013-02-22','M',2),(10,'Eduardo','2000-12-24','M',2),(11,'Amanda','2000-03-15','F',2),(12,'Kleber','1980-06-17','M',2),(13,'Banana','0222-02-22','M',2),(14,'Pedro','1999-11-11','M',6),(15,'Amanda','1888-11-11','F',6);
/*!40000 ALTER TABLE `habitante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `habitante_residencia`
--

DROP TABLE IF EXISTS `habitante_residencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `habitante_residencia` (
  `habitante_id` int(11) DEFAULT NULL,
  `residencia_id` int(11) DEFAULT NULL,
  KEY `habitante_id` (`habitante_id`),
  KEY `residencia_id` (`residencia_id`),
  CONSTRAINT `habitante_residencia_ibfk_1` FOREIGN KEY (`habitante_id`) REFERENCES `habitante` (`habitante_id`),
  CONSTRAINT `habitante_residencia_ibfk_2` FOREIGN KEY (`residencia_id`) REFERENCES `residencia` (`residencia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habitante_residencia`
--

LOCK TABLES `habitante_residencia` WRITE;
/*!40000 ALTER TABLE `habitante_residencia` DISABLE KEYS */;
INSERT INTO `habitante_residencia` VALUES (1,15),(2,15),(1,16),(2,16),(7,2),(7,15),(7,16),(5,17),(8,17),(9,17),(10,17),(14,20),(15,20);
/*!40000 ALTER TABLE `habitante_residencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `residencia`
--

DROP TABLE IF EXISTS `residencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `residencia` (
  `residencia_id` int(11) NOT NULL AUTO_INCREMENT,
  `residencia_rua` varchar(200) DEFAULT NULL,
  `residencia_numero` int(11) DEFAULT NULL,
  `residencia_complemento` varchar(30) DEFAULT NULL,
  `res_cidade_id` int(11) DEFAULT NULL,
  `res_habitante_id` int(11) DEFAULT NULL,
  `res_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`residencia_id`),
  KEY `residencia_ibfk_1` (`res_cidade_id`),
  KEY `residencia_ibfk_2` (`res_habitante_id`),
  KEY `residencia_ibfk_3` (`res_user_id`),
  CONSTRAINT `residencia_ibfk_1` FOREIGN KEY (`res_cidade_id`) REFERENCES `cidade` (`cidade_id`) ON DELETE CASCADE,
  CONSTRAINT `residencia_ibfk_2` FOREIGN KEY (`res_habitante_id`) REFERENCES `habitante` (`habitante_id`) ON DELETE CASCADE,
  CONSTRAINT `residencia_ibfk_3` FOREIGN KEY (`res_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `residencia`
--

LOCK TABLES `residencia` WRITE;
/*!40000 ALTER TABLE `residencia` DISABLE KEYS */;
INSERT INTO `residencia` VALUES (2,'AWDAWD',123,'AWDAS',1,NULL,NULL),(15,'wdawd',213,'123',1,NULL,NULL),(16,'awdwd',123,'wdawd',1,NULL,NULL),(17,'Rua das Flores',12,'ap. 71',1,NULL,2),(20,'Rua das Flores',300,'ap. 71',1,NULL,6);
/*!40000 ALTER TABLE `residencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(30) DEFAULT NULL,
  `user_senha` varchar(150) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_status` tinyint(1) DEFAULT NULL,
  `user_codigo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'bruno','202cb962ac59075b964b07152d234b70','email@email',1,NULL),(2,'kleber','202cb962ac59075b964b07152d234b70','email',1,'d41d8cd98f00b204e9800998ecf8427e'),(3,'kleber','202cb962ac59075b964b07152d234b70','brunosscussi@gmail.com',0,'d41d8cd98f00b204e9800998ecf8427e'),(4,'kleber','202cb962ac59075b964b07152d234b70','brunosscussi@gmail.com',0,'d41d8cd98f00b204e9800998ecf8427e'),(5,'kleber','202cb962ac59075b964b07152d234b70','brunosscussi@gmail.com',0,'d41d8cd98f00b204e9800998ecf8427e'),(6,'UsuarioLegal','202cb962ac59075b964b07152d234b70','brunosscussi@gmail.com',1,'9c3a20c4b2174e858bb8336f1e6c0aa2'),(7,'UsuarioLegal','202cb962ac59075b964b07152d234b70','brunosscussi@gmail.com',1,'9c3a20c4b2174e858bb8336f1e6c0aa2');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculo`
--

DROP TABLE IF EXISTS `veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `veiculo` (
  `veiculo_id` int(11) NOT NULL AUTO_INCREMENT,
  `veiculo_nome` varchar(30) DEFAULT NULL,
  `veiculo_consumo` double DEFAULT NULL,
  `vei_residencia_id` int(11) DEFAULT NULL,
  `vei_habitante_id` int(11) DEFAULT NULL,
  `vei_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`veiculo_id`),
  KEY `vei_residencia_id` (`vei_residencia_id`),
  KEY `vei_habitante_id` (`vei_habitante_id`),
  KEY `vei_user_id` (`vei_user_id`),
  CONSTRAINT `veiculo_ibfk_1` FOREIGN KEY (`vei_residencia_id`) REFERENCES `residencia` (`residencia_id`),
  CONSTRAINT `veiculo_ibfk_2` FOREIGN KEY (`vei_habitante_id`) REFERENCES `habitante` (`habitante_id`),
  CONSTRAINT `veiculo_ibfk_3` FOREIGN KEY (`vei_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculo`
--

LOCK TABLES `veiculo` WRITE;
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` VALUES (1,'wdawd',12,NULL,NULL,2),(2,'Uno',15,NULL,NULL,2),(3,'Uno Flex',12,NULL,NULL,6);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculo_residencia`
--

DROP TABLE IF EXISTS `veiculo_residencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `veiculo_residencia` (
  `veiculo_id` int(11) DEFAULT NULL,
  `residencia_id` int(11) DEFAULT NULL,
  KEY `veiculo_id` (`veiculo_id`),
  KEY `residencia_id` (`residencia_id`),
  CONSTRAINT `veiculo_residencia_ibfk_1` FOREIGN KEY (`veiculo_id`) REFERENCES `veiculo` (`veiculo_id`),
  CONSTRAINT `veiculo_residencia_ibfk_2` FOREIGN KEY (`residencia_id`) REFERENCES `residencia` (`residencia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculo_residencia`
--

LOCK TABLES `veiculo_residencia` WRITE;
/*!40000 ALTER TABLE `veiculo_residencia` DISABLE KEYS */;
INSERT INTO `veiculo_residencia` VALUES (1,17),(3,20);
/*!40000 ALTER TABLE `veiculo_residencia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-06  3:05:53
