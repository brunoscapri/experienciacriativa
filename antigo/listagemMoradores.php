<?php require_once("./conexao.php") ?>
<?php 
    $select = "SELECT * FROM habitante";
    $lista_habitantes = mysqli_query($conexao, $select);
?> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/funcBar.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/bar.css">
</head>
<body>
    <h1>Moradores</h1>
    <div>
    <?php  while($linha = mysqli_fetch_assoc($lista_habitantes)){  ?>
        <div class="users">
            <table>
                <tr>
                    <td>
                        <h2><?php echo "Nome: "; ?></h2><h4><?php echo $linha["nome_habitante"];?></h4>
                        <h2><?php echo "Data: "; ?></h2><h4><?php echo $linha["data_nascimento"];?></h4>
                        <h2><?php echo "Gênero: "; ?></h2><h4><?php echo $linha["genero"];?></h4>
                    </td>
                    <td>
                        <img src="./imagens/avatar.png" id="imgAvatar">
                    </td>
                </tr>
            </table>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php }?>
    </div>
    <ul class="menu">

    <li title="home"><a href="#" class="menu-button home">menu</a></li>

    <li title="search"><a href="#" class="search">search</a></li>
    <li title="pencil"><a href="#" class="pencil">pencil</a></li>
    <li title="about"><a href="#" class="active about">about</a></li>
    <li title="archive"><a href="#" class="archive">archive</a></li>
    <li title="contact"><a href="#" class="contact">contact</a></li>
    </ul>

    <ul class="menu-bar">
        <!--<li><a href="#">Menu</a></li>-->
        <li><a href="./main.php">Home</a></li>
        <li><a href="./listagemResidencias.php">Residências</a></li>
        <li><a href="./listagemMoradores.php" class="menu-button">Moradores</a></li>
        <li><a href="./listagemAparatos.php">Eletrodomésticos</a></li>
    </ul>
</body>
</html>