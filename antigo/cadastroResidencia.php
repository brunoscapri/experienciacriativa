<?php require_once("./php/insere-residencia.php");?> 
<?php 
	$select = "SELECT nome_cidade, idcidade FROM cidade";
	$lista_cidades = mysqli_query($conexao, $select);
?>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/funcoesResidencia.js"></script>
		<link rel="stylesheet" href="css/estilo.css">
		
	</head>
	<body>


		
		<h1>Cadastrar Residência</h1>
		<div class="wpcf7" id="wpcf7-f156-p143-o1 formwrap">
			<form>
				<p>
				   <span class="wpcf7-form-control-wrap Name">
					 <input type="text" name="rua" id="rua" value="" size="40" class="nameinput wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Rua">
				  </span>

				  <span class="wpcf7-form-control-wrap Email">
					<input type="number" name="numero" id="numero" size="40" class="emailinput wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Numero">
                  </span>
                  <span class="wpcf7-form-control-wrap Email">
                        <input type="text" name="complemento" id="complemento" size="40" class="emailinput wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Complemento">
                      </span>
				  
				  <span class="">
					<select name="cidade" id="cidade" class="indent wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                        <option value="" disabled selected>Cidade</option>
												<?php
														while($linha = mysqli_fetch_assoc($lista_cidades)){ 
												?>
					    					<option value="<?php echo $linha["idcidade"]?>"><?php echo $linha["nome_cidade"] ?></option>
												<?php
														}
												?>
					</select>
				  </span>
				  
					<button id="bGravar" class="wpcf7-form-control wpcf7-submit btn">Cadastrar</button>
					
				  <img class="ajax-loader" src="http://www.jordancundiff.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
			  </p>
			  <div class="wpcf7-response-output wpcf7-display-none">
			  </div>
		  </form>
		</div>
		<table class="tab" id="tableListaResidencia">	</table>
	</body>
</html>