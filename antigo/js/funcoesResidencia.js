$(document).ready(function(){

	fLocalEventosClick();

});


function fLocalEventosClick(){

	$("#bGravar").click(function(){
		
		fLocalComunicaServidor('form', 'insere-residencia');
		return false;
	});
	$("#bListar").click(function(){

		fLocalComunicaServidor('form', 'lista-residencia');
		return false;
	});



}


function fLocalComunicaServidor(formulario, file){

	var dados = $(formulario).serialize();
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "php/"+file+".php",
		data: dados,
		success:function(array){
			if(array["funcao"] == "lista-residencia"){
				if(array["status"] == "s"){

					var conteudo_tabela = "";

					console.log(array["residencia"]);
					
					for(var i = 0; i < array["residencia"].length; i++){

						conteudo_tabela += "<tr>";
						conteudo_tabela += "<td>" + array["residencia"][i]["rua"] + "</td>";
						conteudo_tabela += "<td>" + array["residencia"][i]["numero"] + "</td>";
						conteudo_tabela += "<td>" + array["residencia"][i]["complemento"] + "</td>";
						conteudo_tabela += "<td>" + array["residencia"][i]["cidade"] + "</td>";
						conteudo_tabela += "</tr>";

					}

					$("#tableListaResidencia").html(conteudo_tabela);
				}
			}
		}
	});
}
