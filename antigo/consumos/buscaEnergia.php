<?php
require_once("../conexao.php");

header('Content-Type: application/json');

$mysqli = new mysqli($endereco, $usuario_banco, $senha_banco, $nome_banco);

if(!$mysqli){
  die("Connection failed: " . $mysqli->error);
}


$query = sprintf("SELECT residencia_id, data, consumo from consumo_por_residencia where residencia_id = 1");


$result = $mysqli->query($query);


$data = array();
foreach ($result as $row) {
  $data[] = $row;
}


$result->close();


$mysqli->close();


print json_encode($data);

?>