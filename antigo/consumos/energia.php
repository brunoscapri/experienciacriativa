
<?php require_once("../conexao.php")?>
<?php
     $select = "SELECT SUM(consumo_aparato), nome_logradouro, num_logradouro, complemento FROM tipo_aparato_domestico JOIN aparato_domestico ON aparato_domestico.id_tipo_aparato_domestico = tipo_aparato_domestico.id_tipo_aparato_domestico JOIN residencia ON aparato_domestico.residencia_id_residencia = residencia.id_residencia GROUP BY nome_logradouro, num_logradouro, complemento";
     $lista_consumos = mysqli_query($conexao, $select);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/funcBar.js"></script>
    <link rel="stylesheet" href="../css/estilo.css">
	<link rel="stylesheet" href="../css/bar.css">
</head>
<body>
    <h1>Energia Diária</h1>

    <div>
    <?php  while($linha = mysqli_fetch_assoc($lista_consumos)){  ?>
        <div class="users">
            <table>
                <tr>
                    <td>
                        <?php $consumo =  ($linha["SUM(consumo_aparato)"]/1000) * 24 ?>
                        <h2><?php echo "Residência: "; ?></h2><h4><?php echo $linha["nome_logradouro"].", ".$linha["num_logradouro"]. " - " . $linha["complemento"] ;?></h4>
                        <h2><?php echo "Consumo total: "; ?></h2><h4><?php echo $consumo."KWh";?></h4>
                    </td>
                    <td>
                        <img src="../imagens/aparatos.png" id="imgAvatar">
                    </td>
                </tr>
                <tr>
                    <td>
                    <button onclick="location.href='energiaGrafico.html'" class=" btn" style="background-color: #428ff4;">Gráfico</button>
                    </td>
                </tr>
            </table>
            
        </div>
        
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php }?>
    </div>









    <ul class="menu">

    <li title="home"><a href="#" class="menu-button home">menu</a></li>

    <li title="search"><a href="#" class="search">search</a></li>
    <li title="pencil"><a href="#" class="pencil">pencil</a></li>
    <li title="about"><a href="#" class="active about">about</a></li>
    <li title="archive"><a href="#" class="archive">archive</a></li>
    <li title="contact"><a href="#" class="contact">contact</a></li>
    </ul>

    <ul class="menu-bar">
        <!--<li><a href="#">Menu</a></li>-->
        <li><a href="../main.php">Home</a></li>
        <li><a href="../listagemResidencias.php">Residências</a></li>
        <li><a href="../listagemMoradores.php">Moradores</a></li>
        <li><a href="../listagemAparatos.php" class="menu-button">Eletrodomésticos</a></li>
    </ul>
</body>
</html>