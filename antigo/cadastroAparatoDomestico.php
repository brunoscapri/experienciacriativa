<?php
	require_once("./php/insere-aparato.php");
	$select = "SELECT id_residencia ,nome_logradouro, num_logradouro, complemento FROM residencia";
	$selectAparato = "SELECT id_tipo_aparato_domestico, desc_aparato_domestico FROM tipo_aparato_domestico";
	$lista_residencias = mysqli_query($conexao, $select);
	$lista_aparatos = mysqli_query($conexao, $selectAparato);
?>
<!DOCTYPE html>
<meta charset="utf-8">
<html>
	<head>

		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/funcaoAparato.js"></script>
		<link rel="stylesheet" href="css/estilo.css">

	</head>
	<body>

		<br>
<h1>Aparatos</h1>
		<br>
			<br>
				<br>
					<br>
						<br>
		<div class="wpcf7" id="wpcf7-f156-p143-o1 formwrap">
			<form>
				<p>

				  <span class="wpcf7-form-control-wrap Subject ">
						<select name="residencia" id="residencia" class="indent wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
							<option value="" disabled selected>Residência</option>
							<?php while($linha = mysqli_fetch_assoc($lista_residencias)){?>
							<option value="<?php echo $linha["id_residencia"]?>">
							<?php echo $linha["nome_logradouro"];
								  echo ", ";
								  echo $linha["num_logradouro"];
								  echo " - ";
								  echo $linha["complemento"];
							?>
							</option>
							<?php }?>
						</select>
          </span>
                    
          <span class="wpcf7-form-control-wrap Name">
		  <select name="aparato" id="aparato" class="indent wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
							<option value="" disabled selected>Eletrodoméstico</option>
							<?php while($linha1 = mysqli_fetch_assoc($lista_aparatos)){?>
							<option value="<?php echo $linha1["id_tipo_aparato_domestico"]?>">
							<?php echo utf8_decode($linha1["desc_aparato_domestico"]);?>
							</option>
							<?php }?>
						</select>
          </span>
					
					<button id="bGravar" class="wpcf7-form-control wpcf7-submit btn">Cadastrar</button>
			  </p>
			</form>
			</div>
			<table class="tab" id="tableListaAparatos" ></table>
	</body>
</html>